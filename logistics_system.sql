/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 50729
 Source Host           : localhost:3306
 Source Schema         : logistics_system

 Target Server Type    : MySQL
 Target Server Version : 50729
 File Encoding         : 65001

 Date: 23/12/2021 15:21:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cars
-- ----------------------------
DROP TABLE IF EXISTS `cars`;
CREATE TABLE `cars`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `car_num` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `max_weight` double(8, 2) NOT NULL,
  `car_state` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `cars_uk`(`car_num`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cars
-- ----------------------------
INSERT INTO `cars` VALUES (1, '辽B.12345', 800.00, '未满载');
INSERT INTO `cars` VALUES (2, '辽A.67890', 500.00, '未满载');

-- ----------------------------
-- Table structure for logistics
-- ----------------------------
DROP TABLE IF EXISTS `logistics`;
CREATE TABLE `logistics`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `ord_id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `car_id` int(10) NOT NULL,
  `path_id` int(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `logistics_fk_ord_id`(`ord_id`) USING BTREE,
  INDEX `logistics_fk_car_id`(`car_id`) USING BTREE,
  INDEX `logistics_fk_path_id`(`path_id`) USING BTREE,
  CONSTRAINT `logistics_fk_car_id` FOREIGN KEY (`car_id`) REFERENCES `cars` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `logistics_fk_ord_id` FOREIGN KEY (`ord_id`) REFERENCES `orders` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT,
  CONSTRAINT `logistics_fk_path_id` FOREIGN KEY (`path_id`) REFERENCES `paths` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of logistics
-- ----------------------------
INSERT INTO `logistics` VALUES (2, 'a123', 1, 1);
INSERT INTO `logistics` VALUES (3, 'a235', 2, 2);
INSERT INTO `logistics` VALUES (4, 'a222', 1, 2);
INSERT INTO `logistics` VALUES (6, 'a245', 1, 1);
INSERT INTO `logistics` VALUES (7, 'ase34', 2, 2);

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders`  (
  `id` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(10) NOT NULL,
  `consignee` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `con_address` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `con_phone` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` datetime(0) NOT NULL,
  `type` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `weight` double(8, 2) NOT NULL,
  `ord_money` double(8, 2) NOT NULL,
  `ord_state` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `orders_fk_user_id`(`user_id`) USING BTREE,
  CONSTRAINT `orders_fk_user_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of orders
-- ----------------------------
INSERT INTO `orders` VALUES ('0d8bd', 2, 'qwe', 'a省b市c区', '17956564565', '2021-11-18 21:09:44', '易燃品', 5.00, 50.00, '用户创建');
INSERT INTO `orders` VALUES ('a123', 2, '张三', '北京市朝阳区', '12345678900', '2021-11-12 09:37:49', '日用品', 10.00, 30.00, '订单完成');
INSERT INTO `orders` VALUES ('a222', 2, '张三', 'a市b区', '1234567890', '2021-11-15 18:17:27', '日用品', 10.00, 20.00, '订单完成');
INSERT INTO `orders` VALUES ('a235', 1, '李四', '齐齐哈尔市a区', '13456587989', '2021-11-12 15:56:47', '易碎品', 5.00, 30.00, '运送中');
INSERT INTO `orders` VALUES ('a245', 2, 'bb', 'asd', '1234567890', '2021-11-15 23:17:32', '电子用品', 5.00, 100.00, '订单完成');
INSERT INTO `orders` VALUES ('ase34', 4, 'ee', 'adad', '12346789741', '2021-11-19 10:41:59', '易碎品', 10.00, 30.00, '订单完成');

-- ----------------------------
-- Table structure for paths
-- ----------------------------
DROP TABLE IF EXISTS `paths`;
CREATE TABLE `paths`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `path` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `path_start` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `path_end` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `path_way` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `path_money` double(8, 2) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of paths
-- ----------------------------
INSERT INTO `paths` VALUES (1, '大连-北京', '大连', '北京', '锦州、秦皇岛，aaa,bbb', 10.00);
INSERT INTO `paths` VALUES (2, '大连-齐齐哈尔', '大连', '齐齐哈尔', '沈阳', 20.00);
INSERT INTO `paths` VALUES (3, '大连-威海', '大连市', '威海市', '锦州，天津', 80.00);

-- ----------------------------
-- Table structure for roles
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `role` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `roles_uk`(`role`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES (2, '用户');
INSERT INTO `roles` VALUES (1, '管理员');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `address` varchar(60) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `role_id` int(10) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user_uk`(`username`, `phone`) USING BTREE,
  INDEX `users_fk_role_id`(`role_id`) USING BTREE,
  CONSTRAINT `users_fk_role_id` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE RESTRICT ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'admin', 'admin', '123456789', '辽宁省大连市', 1);
INSERT INTO `users` VALUES (2, 'user', 'user', '987654321', '辽宁省锦州市', 2);
INSERT INTO `users` VALUES (4, 'test1', 'test1', '17609324856', 'aaa', 2);

SET FOREIGN_KEY_CHECKS = 1;
