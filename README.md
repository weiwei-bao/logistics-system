# LogisticsSystem

#### 介绍
基于SSM的物流管理系统

#### 软件架构
使用MVC+三层架构设计

### 开发环境
主要语言：JAVA
前端：HTML，CSS，JavaScript，jQuery，Bootstrap；
J2EE：Tomcat9，Spring，SpringMVC，MyBatis；
数据库：MySQL5.7；
开发工具：Jdk1.8，IntelliJ IDEA，Maven3.6，Tomcat，MySQL5.7，NavicatPremium12；
